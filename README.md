asdf-exercism
=============

VM for [Exercism](https://exercism.org/).

Installation:

```
asdf plugin add exercism https://gitlab.com/bheesham/asdf-exercism.git
asdf install exercism latest
asdf global exercism latest
```
